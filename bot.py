from selenium import webdriver
from time import sleep
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import datetime
import os
from dotenv import load_dotenv

load_dotenv()
USERNAME = os.environ.get('INSTA_USER')
PASSWORD = os.environ.get('INSTA_PASS')
PATH = os.environ.get('INSTA_PATH')
hastag = 'aniesbaswedan'


class Bot():

    url_list = []

    def __init__(self):
        self.login()
        self.get_links()


    def login(self):
        self.options = webdriver.ChromeOptions()
        # self.options.add_argument("--headless")
        self.options.add_argument('--no-sandbox')
        self.options.add_argument("--log-level=3")
        self.mobile_emulation = {
            "userAgent": "Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/90.0.1025.166 Mobile Safari/535.19"}
        self.options.add_experimental_option("mobileEmulation", self.mobile_emulation)

        self.driver = webdriver.Chrome(executable_path=PATH, options=self.options)
        self.driver.get('https://www.instagram.com/accounts/login/')
        sleep(5)

        instagram_id_form = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name='username']")))
        instagram_id_form.send_keys(USERNAME)
        sleep(5)

        instagram_pw_form = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name='password']")))
        instagram_pw_form.send_keys(PASSWORD)
        sleep(7)

        login_ok_button = WebDriverWait(self.driver, 2).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "button[type='submit']")))
        login_ok_button.click()
        sleep(7)

        not_now = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//button[contains(text(),'Not Now')]")))
        not_now.click()
        sleep(3)

        not_now = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//button[contains(text(),'Cancel')]")))
        not_now.click()
        sleep(3)

    def get_links(self,):

        url = "https://www.instagram.com/explore/tags/{}/".format(hastag)
        sleep(2) 

        self.driver.get(url)
        sleep(5)

        scroll = 5
        for i in range(scroll):
            self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")


        images = self.driver.find_element(By.TAG_NAME, 'a')

        for url in images.get_attribute('href'):
            url = images.get_attribute('href').strip()
        print(url)

def main():
    my_bot = Bot()

if __name__ == '__main__':
    main()